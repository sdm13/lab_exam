const express =require('express')
const db =require('../db')
const utils=require('../utils')

const router=express.Router()

router.post('/',(request,response)=>{
    const{movie_title,movie_release_date,movie_time,director_name}=request.body

    const query =`
    INSERT INTO Movie
    (Movie_title,Movie_release_date,Movie_time,Director_name)
    VALUES
    ('${movie_title}','${movie_release_date}','${movie_time}','${director_name}') `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})

router.get('/',(request,response)=>{

    const query =`
   SELECT * FROM Movie `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})
router.put('/:movie_id',(request,response)=>{
    const {movie_id} =request.params
    const{movie_title,movie_release_date,movie_time,director_name}=request.body
   
    const query =`
     UPDATE Movie 
     SET
     movie_titlr='${movie_title}' where
     movie_id=${movie_id} `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})
router.delete('/:movie_id',(request,response)=>{
    const {movie_id} =request.params
    
    const query =`
    DELETE FROM Movie 
    where
     movie_id=${movie_id} `
    db.execute(query,(error,result)=>{
        response.send(utils.createResult(error,result))
    })
})
module.exports=router